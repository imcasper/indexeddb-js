package casper.indexeddb.core

import org.w3c.dom.events.Event

external interface IDBRequest {
	var error: ((Event) -> dynamic)?
	var onerror: ((Event) -> dynamic)?
	var onsuccess: ((Event) -> dynamic)?
	var readyState: ((Event) -> dynamic)?
	var result: Any?
	var source: Any?/* IDBIndex, IDBObjectStore or IDBCursor*/
	var transaction: IDBTransaction?
}